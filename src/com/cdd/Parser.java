package com.cdd;

import com.cdd.Game.Armor;
import com.cdd.Game.Weapon;
import com.cdd.Users.User;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Parser {
    public static List<User> ParseCSV(String path)
    {
        List<User> Users = new ArrayList<>();
        CSVParser csvParser = new CSVParserBuilder().withSeparator(';').build(); // custom separator
        try(CSVReader reader = new CSVReaderBuilder(
                new FileReader(path))
                .withCSVParser(csvParser)   // custom CSV parser
                .withSkipLines(1)           // skip the first line, header info
                .build()){
            List<String[]> r = reader.readAll();
            r.forEach(x -> Users.add(new User(x[0], x[1], Integer.parseInt(x[2]),  Integer.parseInt(x[3]), Weapon.parseWeapon(x[4]), Armor.parseArmor(x[5]))));
        } catch (IOException | CsvException e) {
            System.out.println("Database error");
            e.printStackTrace();
        }

        for (User user : Users) {
            System.out.println(user.getFirstname());
        }
        return Users;
    }
}
