package com.cdd.Game;

public class Weapon {
    public WeaponType Weapon;
    public enum WeaponType {
        Hache, Dague, Sabre, Hallebarde, Épée
    }
    public void setWeapon(WeaponType w)
    {
        this.Weapon = w;
    }
    public static WeaponType parseWeapon(String weapon) {
        switch (weapon) {
            case "Hache": {
                return WeaponType.Hache;
            }
            case "Dagues": {
                return WeaponType.Dague;
            }
            case "Sabre": {
                return WeaponType.Sabre;
            }
            case "Hallebarde": {
                return WeaponType.Hallebarde;
            }
            case "Epee": {
                return WeaponType.Épée;
            }
        }
        return null;
    }
}

