package com.cdd.Game;

public class Armor {
    public ArmorType armor;
    public enum ArmorType {
       Tissu, Cuir, Mailles, Plaques, Gambison
    }
    public void setArmor(ArmorType a)
    {
        this.armor = a;
    }

    public static ArmorType parseArmor(String armor)
    {
        switch (armor) {
            case "Tissu" : {
                return ArmorType.Tissu;
            }
            case "Cuir" : {
                return ArmorType.Cuir;
            }
            case "Mailles" : {
                return ArmorType.Mailles;
            }
            case "Plaques" : {
                return ArmorType.Plaques;
            }
            case "Gambison" : {
                return ArmorType.Gambison;
            }
        }
        return null;
    }
}
