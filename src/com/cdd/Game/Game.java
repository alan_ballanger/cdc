package com.cdd.Game;

import com.cdd.Users.User;

import java.util.Arrays;
import java.util.List;

public class Game {

    public Game() {

    }

    public void Start(List<User> TeamA, List<User> TeamB)
    {
        float average_weight_a=0;
        for (User u : TeamA){
            average_weight_a += u.getWeight();
        }
        average_weight_a /= TeamA.size();

        float average_weight_b=0;
        for (User u : TeamB){
            average_weight_b += u.getWeight();
        }
        average_weight_b /= TeamA.size();

        float average_year_a=0;
        for (User u : TeamA){
            average_year_a += u.getAdhesion_year();
        }
        average_year_a /= TeamA.size();
        average_year_a = 2021 - average_year_a;

        float average_year_b=0;
        for (User u : TeamB){
            average_year_b += u.getAdhesion_year();
        }
        average_year_b /= TeamA.size();
        average_year_b = 2021 -  average_year_b;

        System.out.println("THE GAME HAS STARTED WITH TEAM A AND TEAM B");
        System.out.println("\n EQUIPE A");
        for (User u : TeamA){
            System.out.println( "\n Nom : "+ u.getFirstname() + "\n Prénom : "+ u.getLastname());
        }
        System.out.println("\n L'équipe A est composée de "+TeamA.size()+" joueurs. \n Poids moyen : " + average_weight_a + "Kg \n Ancienneté moyenne : "+average_year_a + " ans");

        System.out.println("\n EQUIPE B");
        for (User u : TeamB){
            System.out.println( "\n Nom : "+ u.getFirstname() + "\n Prénom : "+ u.getLastname());
        }
        System.out.println("\n L'équipe B est composée de "+TeamB.size()+" joueurs. \n Poids moyen : " + average_weight_b + "Kg \n Ancienneté moyenne : "+average_year_b + " ans");

    }

}
