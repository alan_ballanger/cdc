package com.cdd.Game;

import com.cdd.Users.User;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class Matchmaking {
    List<User> teamA;
    List<User> teamB;
    List<List<User>> _Teams;
    public Matchmaking(List<User> Users) {
        //checkErrors
        createTeams(Users);
    }
    private void createTeams(List<User> Users)
    {
       _Teams = new ArrayList<>();
        System.out.println("Creating teams...");

        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int average_year_a = 0;
        int average_year_b = 0;
        teamA = new ArrayList<>();
        teamB = new ArrayList<>();

        Users.sort((d1, d2) -> {
            return d2.getWeight() - d1.getWeight();
        });
        for (int i = 0; i < Users.size(); i++) {
            if (i % 2 == 0) {
                teamA.add(Users.get(i));
            } else {
                teamB.add(Users.get(i));
            }
        }

        User a;
        User b;

        int cpt = 0;
        while ( (Math.abs(AverageYear(teamA)-AverageYear(teamB)) >= 1 && cpt < teamA.size()))  {
            if (AverageYear(teamA) > AverageYear(teamB)) {
                if (teamA.get(cpt).getAdhesion_year() > teamB.get(cpt).getAdhesion_year()) {
                    a = teamA.get(cpt);
                    b = teamB.get(cpt);
                    teamB.set(cpt, a);
                    teamA.set(cpt, b);
                }
            }else if (AverageYear(teamA) < AverageYear(teamB)){
                if (teamA.get(cpt).getAdhesion_year() < teamB.get(cpt).getAdhesion_year()) {
                    a = teamA.get(cpt);
                    b = teamB.get(cpt);
                    teamB.set(cpt, a);
                    teamA.set(cpt, b);
                }
            }
            cpt++;
        }

        _Teams.add(teamA);
        _Teams.add(teamB);
    }

    public List<List<User>> getTeams()
    {
        return _Teams;
    }
    
    public int AverageYear(List<User> listUser){
        int totalYear = 0;
        for (User u : listUser){
            totalYear += u.getAdhesion_year();
        }
        return totalYear /= listUser.size();
    }
}
