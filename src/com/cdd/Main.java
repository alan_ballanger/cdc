package com.cdd;

import com.cdd.Game.Armor;
import com.cdd.Game.Weapon;
import com.cdd.Users.User;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        System.out.println("Starting program...");

        List<User> Users = Parser.ParseCSV("src/test/FichierDeTestFictif.csv");
        System.out.println("Players OK...");
        App.Start(Users);
    }

}