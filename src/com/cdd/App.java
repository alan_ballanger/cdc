package com.cdd;

import com.cdd.Game.Game;
import com.cdd.Game.Matchmaking;
import com.cdd.Users.User;

import java.util.List;

public class App {
    public static void Start(List<User> Users)
    {
        Matchmaking mm = new Matchmaking(Users);
        Game g = new Game();
        List<List<User>> Teams = mm.getTeams();
        List<User> TeamA = Teams.get(0);
        List<User> TeamB = Teams.get(1);
        g.Start(TeamA, TeamB);

    }
}
