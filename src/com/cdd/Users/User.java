package com.cdd.Users;

import com.cdd.Game.Armor;
import com.cdd.Game.Weapon;

public class User {

    private String firstname;
    private String lastname;
    private Integer weight;
    private Integer adhesion_year;
    private Armor.ArmorType armor;
    private Weapon.WeaponType weapon;

    public User( String lastname, String firstname, Integer weight, Integer adhesion_year, Weapon.WeaponType weapon, Armor.ArmorType armor) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.weight = weight;
        this.adhesion_year = adhesion_year;
        this.armor = armor;
        this.weapon = weapon;
    }


    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getAdhesion_year() {
        return adhesion_year;
    }

    public void setAdhesion_year(Integer adhesion_year) {
        this.adhesion_year = adhesion_year;
    }

    public Armor.ArmorType getArmor() {
        return armor;
    }

    public void setArmor(Armor.ArmorType armor) {
        this.armor = armor;
    }

    public Weapon.WeaponType getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon.WeaponType weapon) {
        this.weapon = weapon;
    }
}


