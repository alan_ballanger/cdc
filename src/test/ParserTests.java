package test;

import com.cdd.Parser;
import com.cdd.Users.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class ParserTests {

    @Test
    public void TestParseCSV()
    {
        List<User> Users = Parser.ParseCSV("./src/test/FichierDeTestFictif.csv");
        Assertions.assertEquals("Paul", Users.get(0).getFirstname());
    }
}
