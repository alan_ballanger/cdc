package test;

import com.cdd.Game.Armor;
import com.cdd.Game.Weapon;
import com.cdd.Users.User;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GameTests {
    private User user1 = new User("alan", "ballanger", 69, 2020, Weapon.WeaponType.Dague, Armor.ArmorType.Mailles);
    private User user2 = new User("bouteille", "eau", 100, 2017, Weapon.WeaponType.Hallebarde, Armor.ArmorType.Gambison);
    private User user3 = new User("bureau", "quichargepas", 92, 2014, Weapon.WeaponType.Hache, Armor.ArmorType.Cuir);

    private User user4 = new User("emilie", "rata", 80, 2016, Weapon.WeaponType.Hache, Armor.ArmorType.Cuir);
    private User user5 = new User("robert", "beauf", 122, 2019, Weapon.WeaponType.Hallebarde, Armor.ArmorType.Gambison);
    private User user6 = new User("marc", "dupont", 65, 2021, Weapon.WeaponType.Hache, Armor.ArmorType.Mailles);

    @Test
    public void IsSeniorityCalculationValid(){
        List<User> user_list = new ArrayList<>(Arrays.asList(user1, user2, user3));
        Team team = new Team(user_list);
        assertEquals(2017, team.getTeamSeniority());
    }

    @Test
    public void IsWeightCalculationValid(){
        List<User> user_list = new ArrayList<>(Arrays.asList(user1, user2, user3));
        Team team = new Team(user_list);
        assertEquals(87, team.getTeamAmountWeights());
    }

    @Test
    public void AreBothTeamAmountSimilar(){
        List<User> user_list = new ArrayList<>(Arrays.asList(user1, user2, user3,user4, user5, user6));
        Game game = new Game(user_list);
        assertTrue(1 >= team.get(0).getUserList().size() - team.get(1).getUserList().size());
    }

    @Test
    public void AreBothTeamSenioritySimilar(){
        List<User> user_list = new ArrayList<>(Arrays.asList(user1, user2, user3,user4, user5, user6));
        Game game = new Game(user_list);
        assertTrue(20 >= Math.abs(team.get(0).getTeamSeniority()-team.get(1).getTeamSeniority()));
    }

    @Test
    public void AreBothTeamWeightsSimilar(){
        List<User> user_list = new ArrayList<>(Arrays.asList(user1, user2, user3,user4, user5, user6));
        Game game = new Game(user_list);
        assertTrue(50 >= Math.abs(team.get(0).getTeamAmountWeights()-team.get(1).getTeamAmountWeights()));
    }
}